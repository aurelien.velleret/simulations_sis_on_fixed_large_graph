# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:20:05 2022

@author: Aurélien Velleret
"""


#%%
import numpy as np
import pandas as pd

#version =input("What is the caracteristic number for the figures?\n")
version = 43
summary_parameters = pd.DataFrame([version], columns = ['version']) 

#NI = input("How many individuals are to be considered?\n")
NI = 2000
summary_parameters["Nbr_indiv_ref"] = NI
#uA =input("How likely it is for individuals to be initially infected?\n")
uA = 1.
summary_parameters['p_init_infected'] = uA

#gamma =input("What is the rate of remission (uniform here)?\n")
gamma = 0.7
summary_parameters['rate_remission'] = gamma
#epsN =input("What is the scale of meeting interactions?\n")
#alpha =input("What is the power-law for this scaling?\n")
alpha = 0.6
summary_parameters['alpha'] = alpha
epsN = 1/NI**alpha

#Here I rather consider the sparse graph situation, where epsN is of order 1


#wI0 =input("How likely it is for two connected individuals to transmit an infection?\n")
wI0 = 1.2/epsN
summary_parameters['w_I_ref'] = wI0
#wE0 =input("How likely it is for two individuals to be connected?\n")
wE0 = 3/wI0
summary_parameters['w_E_ref'] = wE0
summary_parameters['expected_equilibrium'] = 1-gamma/(wI0*wE0)

#t_f = input("What is the targetted final time? (initiation at time 1)\n")
t_f = 80.
summary_parameters['final_time'] = t_f
#t_J = input("What is the targetted duration between observations? \n")
t_J = 0.2
summary_parameters['duration_btw_observations'] = t_J
#t_o = input("What is the time for the start of observation?")
#relate to the warm-up time
t_o = 20.
summary_parameters['duration_bfr_start_observations'] = t_o

#NN = input("By how which power 2 do you wish to amplify the size?")
NN = 100
#from constraint on computation time, I avoid going beyond 20k indiv
NIK = (10**np.linspace(3, 4, NN)).astype(int)
summary_per_pop_size = pd.DataFrame(NIK, columns =  ['Nbr_indiv'])

summary_parameters.to_csv('summary_parameters_v{:d}.csv'.format(version))
summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))

#%%
