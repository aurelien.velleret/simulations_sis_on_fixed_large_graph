# -*- coding: utf-8 -*-
"""
Created on Sun Apr  3 16:37:07 2022

@author: alter
"""

#%%
import numpy as np
import numpy.random as npr
import pandas as pd

#v3 -> case of a single population covered for the generation of AMI
#v4 -> improvement of the code for larger NI thanks to random.generator.choice
#%%
#function AMI_Def_1 : definition of the weigthed sparse graph of interactions, 
#undifferentiated individuals

###INPUT
##NI : total number of individuals

##epsN : the scale of meeting interactions

##edges probabilities : aE

##edges intensities : aI

###OUTPUT
##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

#%%
def AMI_Def_1(NI, epsN, aE,aI):
    rng = np.random.default_rng(12645)
    NLI = npr.poisson(aE/epsN*NI)
    LI = rng.choice(NI*NI, size = NLI, replace = False)
    #a = a % b + b * (a // b) up to roundoff.
    LIT= np.array([LI%NI, LI//NI])
    #filtering by symmetry to select edges by the condition (i<j)
    LIT = LIT[:, LIT[0]<LIT[1]]
    
    #total number of edges
    NE = LIT.shape[1]
    #Adjacency matrix in sparse format, including intensity for any edge
    AMI = pd.DataFrame(np.ones((2*NE, 2), dtype = int), \
                           index=np.arange(2*NE), \
                           columns = ["Origin", "Destination"])
    #between A types
    AMI.iloc[:NE, 0] = LIT[0]
    AMI.iloc[:NE, 1] = LIT[1]
    AMI[["Intensity"]] = aI*epsN

    #symmetry of the edges
    AMI.iloc[NE:, 0] = AMI.iloc[:NE, 1]
    AMI.iloc[NE:, 1] = AMI.iloc[:NE, 0]
    AMI.iloc[NE:, 2] = AMI.iloc[:NE, 2]
    
    return AMI

#%%
#function AMI_Def : definition of the weigthed sparse graph of interactions

###INPUT
##NI : total number of individuals
##NA : number of A types

##epsN : the scale of meeting interactions

##edges probabilities
#aE : btwn A types
#bE : btwn B types
#cE : btwn A and B types

##edges intensities
#aI : btwn A types
#bI : btwn B types
#cI : btwn A and B types

###OUTPUT
##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

#%%
def AMI_Def(NI, NA, epsN, aE, bE, cE, aI, bI, cI):
    NB = NI - NA
    #Identification of the links between two type A individuals
    NLA = npr.poisson(aE/(NI*epsN)*NA*NA)
    rng = np.random.default_rng(12645)
    LA = rng.choice(NA*NA, size = NLA, replace = False)
    #a = a % b + b * (a // b) up to roundoff.
    LAT= np.array([LA%NA, LA//NA])
    #filtering by symmetry to select edges by the condition (i<j)
    LAT = LAT[:, LAT[0]<LAT[1]]
    
    #Identification of the links between two type B individuals
    NLB = npr.poisson(bE/(NI*epsN)*NB*NB)
    LB = npr.choice(NB*NB, size = NLB)
    LBT= np.array([NA + LB%NB, NA + LB//NB])
    #filtering by symmetry to select edges by the condition (i<j)
    LBT = LBT[:, LBT[0]<LBT[1]]
    
    #Identification of the links between type A and type B individuals
    NLAB = npr.poisson(cE/(NI*epsN)*NA*NB)
    LAB = npr.choice(NA*NB, size = NLAB)
    LABT = np.array([LAB%NA, NA + LAB//NA])
    #it is such that (i<j) because the A type individuals are before the B types
    
    
    #total number of edges
    NE_A = LAT.shape[1]
    NE_B = LAT.shape[1] + LBT.shape[1]
    NE_AB = LAT.shape[1] + LBT.shape[1] + LABT.shape[1]
    #Adjacency matrix in sparse format, including intensity for any edge
    AMI = pd.DataFrame(np.ones((2*NE_AB, 2), dtype = int), \
                           index=np.arange(2*NE_AB), \
                           columns = ["Origin", "Destination"])
    #between A types
    AMI.iloc[:NE_A, 0] = LAT[0]
    AMI.iloc[:NE_A, 1] = LAT[1]
    AMI[["Intensity"]] = aI*epsN
    #between B types
    AMI.iloc[NE_A:NE_B, 0] = LBT[0]
    AMI.iloc[NE_A:NE_B, 1] = LBT[1]
    AMI.iloc[NE_A:NE_B, 2] = bI*epsN
    #between A and B types
    AMI.iloc[NE_B:NE_AB, 0] = LABT[0]
    AMI.iloc[NE_B:NE_AB, 1] = LABT[1]
    AMI.iloc[NE_B:NE_AB, 2] = cI*epsN
    
    #symmetry of the edges
    AMI.iloc[NE_AB:, 0] = AMI.iloc[:NE_AB, 1]
    AMI.iloc[NE_AB:, 1] = AMI.iloc[:NE_AB, 0]
    AMI.iloc[NE_AB:, 2] = AMI.iloc[:NE_AB, 2]
    
    return AMI


#%%
#function Step_inf : one step of infection

###INPUT
##is_I : the boolean indicator of size NI for individuals beeing I at this step

##event : index of the event to be considered

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
##is_I2 :  the boolean indicator of size NI for individuals beeing I at the end of the step


#%%
def Step_Inf(is_I, event, AMI, gamma):
    if (event < is_I.size): #it is a remission
        #index of the remitted individual
        i_Rem = event
        #new set of infected individuals
        is_I2 = np.copy(is_I)
        is_I2[i_Rem] = False
    else: #it is an infection
        #index of the activated link
        i_AL = event - is_I.size        
        #index of the potentially infectious individual, in the origin
        i_Ori = AMI.loc[i_AL, "Origin"]
        is_I2 = np.copy(is_I)
        if is_I[i_Ori] == True:
            #index of the potentially infected individual, in the destination
            i_Inf = AMI.loc[i_AL, "Destination"]  
            #new set of infected individuals
            is_I2[i_Inf] = True
    return is_I2
    
#%%
#function Total_inf : a number of infection steps from Step_inf

###INPUT
##N_it : the number of iterations

##is_I : the boolean indicator of size NI for individuals beeing I at the initiation step

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step


###The procedure goes on even without any infected individual,
### rapidly because the conditions of change are never fulfilled
#%%

def Total_Inf(N_it, is_I, AMI, gamma):
    #choice of the potential events
    vp = np.concatenate((np.repeat(gamma, is_I.size), AMI.loc[:, "Intensity"]))
    #change of the time scale through the rate of events
    Svp = np.sum(vp)
    vp = vp/Svp
    rng = np.random.default_rng(12675)
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)
    #setting of the timing
    ##through  the sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0.
    Time = 1 + np.cumsum(T_exp)/Svp
    #initialization
    is_IT = np.zeros((N_it, is_I.size), dtype = bool)
    is_IT[0] = is_I
    for k in np.arange(1, N_it):
        is_IT[k] = Step_Inf(is_IT[k-1], Event[k-1], AMI, gamma)
    return Time, is_IT

#%%
#issue of memory for so large number of iterations

#function Partial_Inf : a number of infection steps from Step_Inf, 
#the data is not recorded between steps (direct triming)

###INPUT
##N_it : the number of iterations

##is_I : the boolean indicator of size NI for individuals beeing I at the initiation step

##tO : the current time at the start of the function


##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

##Svp : rate for total occurrence of events
    
##vp : the renormalised probabilities for indicating the event

###OUTPUT
#Time : the timing at the end of the N_it steps, starting at tO

##is_I2 :  the boolean indicator of size NI for individuals beeing I at the end


#%%
def Partial_Inf(N_it, tO, is_I, AMI, gamma, Svp, vp):
    is_I2 = np.copy(is_I)
    #sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0
    Time = tO + np.sum(T_exp)/Svp
    rng = np.random.default_rng()
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)    
    for k in np.arange(1, N_it):
        is_I2 = Step_Inf(is_I2, Event[k-1], AMI, gamma)
    return Time, is_I2

#%%
#function Jumps_Inf : a number of infection steps from Step_inf, 
#where jumps correspond to the period where no data is kept on the process
#the code is here still nearly exact, only the cost of storage is partly lifted

###INPUT
##t_f>1 : expected final time (initiation at time 1.)
#the actual one will be an approximation expressed through the number of iterations

##t_J : expected time of jump
#the actual one will be an approximation expressed through the number of iterations

##is_I : the boolean indicator of size NI for individuals beeing I at the initiation step

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step


#%%
def Jumps_Inf(t_f, t_J, is_I, AMI, gamma):
    #calibration of the duration in terms of a number of iterations
    N_it = 1000
    Time, is_IT = Total_Inf(N_it, is_I, AMI, gamma)
    if not(np.any(is_IT[-1])):#the infection had stopped previously
        Nf = np.sum(Time >0)
        print("end of the infection at time {:.2f}".format(Time[Nf-1]))
        return Time[:Nf], is_IT[:Nf]
    else: 
        N_J = int(t_J/(Time[-1]-1)*N_it) 
        N_it2= int((t_f-1) /t_J)+1
        #calibration completed, we go on with the initialisation
        Time = np.zeros(N_it2)
        is_IT = np.zeros((N_it2, is_I.size), dtype = bool)
        k = 1
        Time[0] = 1.
        is_IT[0] = np.copy(is_I)
        #for the choice of the potential events
        vp = np.concatenate((np.repeat(gamma, is_I.size), AMI.loc[:, "Intensity"]))
        #change of the time scale through the rate of events
        Svp = np.sum(vp)
        vp = vp/Svp
        #iteration procedure of steps without memory
        k = 1
        while (k < N_it2)&(np.any(is_IT[k-1])):
            Time[k], is_IT[k]= Partial_Inf(N_J, Time[k-1], is_IT[k-1], AMI, gamma, Svp, vp)
            k = k+1
        Nf = np.sum(Time >0)
        return Time[:Nf], is_IT[:Nf]
    
#%%
##Graph exploration for connectivity
###INPUT

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##NI : the number of individuals

###OUTPUT

#the number of connected components

#the function from the set of individuals to the set of components
#%%
def Graph_connect(AMI, NI):
    CoCom = np.arange(NI)
    Edge = np.ones((2, AMI.shape[0]), dtype = int)
    Edge[0] = np.array(AMI["Origin"])
    Edge[1] = np.array(AMI["Destination"])
    #since the graph is undirected, I remove duplicates
    I = (Edge[0]<Edge[1])
    Edge = Edge[:, I]
    while Edge.size >1:
        Com1 = CoCom[Edge[0,0]]
        Com2 = CoCom[Edge[1, 0]]
        if Com1 < Com2:
            CoCom[CoCom == Com2] = Com1
        else:
            CoCom[CoCom == Com1] = Com2
        Edge = Edge[:, CoCom[Edge[0, :]]!=CoCom[Edge[1, :]] ]
    return np.unique(CoCom).size, CoCom
#%%

##Extension avec prise en compte d'un couplage entre conditions extrêmes
#%%
#function Step_Inf_cpl : one step of infection

###INPUT
##is_I1 : the boolean indicator of size NI for individuals beeing I at the initiation step
#first marginal

##is_I2 : the boolean indicator of size NI for individuals beeing I at the initiation step
#second marginal

##event : index of the event to be considered

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
##is_If1 :  the boolean indicator of size NI for individuals beeing I at the end of the step
#first marginal

##is_If2 :  the boolean indicator of size NI for individuals beeing I at the end of the step
#second marginal

#%%
def Step_Inf_cpl(is_I1, is_I2,  event, AMI, gamma):
    if (event < is_I1.size): #it is a remission
        #index of the remitted individual
        i_Rem = event
        #new set of infected individuals
        is_If1 = np.copy(is_I1)
        is_If1[i_Rem] = False
        is_If2 = np.copy(is_I2)
        is_If2[i_Rem] = False
    else: #it is an infection
        #index of the activated link
        i_AL = event - is_I1.size        
        #index of the potentially infectious individual, in the origin
        i_Ori = AMI.loc[i_AL, "Origin"]
        is_If1 = np.copy(is_I1)
        is_If2 = np.copy(is_I2)
        if is_I1[i_Ori] == True:
            #index of the potentially infected individual, in the destination
            i_Inf = AMI.loc[i_AL, "Destination"]  
            #new set of infected individuals
            is_If1[i_Inf] = True
        if is_I2[i_Ori] == True:
            #index of the potentially infected individual, in the destination
            i_Inf = AMI.loc[i_AL, "Destination"]  
            #new set of infected individuals
            is_If2[i_Inf] = True
    return is_If1, is_If2
    
#%%
#function Total_Inf_cpl : a number of infection steps from Step_inf

###INPUT
##N_it : the number of iterations

##is_I1 : the boolean indicator of size NI for individuals beeing I at the initiation step
#first marginal

##is_I2 : the boolean indicator of size NI for individuals beeing I at the initiation step
#second marginal

##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT1 :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step
#first marginal

##is_IT2 :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step
#second marginal

###The procedure goes on even without any infected individual,
### rapidly because the conditions of change are never fulfilled
#%%

def Total_Inf_cpl(N_it, is_I1, is_I2, AMI, gamma):
    #choice of the potential events
    vp = np.concatenate((np.repeat(gamma, is_I1.size), AMI.loc[:, "Intensity"]))
    #change of the time scale through the rate of events
    Svp = np.sum(vp)
    vp = vp/Svp
    rng = np.random.default_rng(12685)
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)
    #setting of the timing
    ##through  the sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0.
    Time = 1 + np.cumsum(T_exp)/Svp
    #initialization
    is_IT1 = np.zeros((N_it, is_I1.size), dtype = bool)
    is_IT1[0] = is_I1
    is_IT2 = np.zeros((N_it, is_I2.size), dtype = bool)
    is_IT2[0] = is_I2
    for k in np.arange(1, N_it):
        is_IT1[k], is_IT2[k]  = Step_Inf_cpl(is_IT1[k-1], is_IT2[k-1],  Event[k-1], AMI, gamma)
    return Time, is_IT1, is_IT2

#%%
#issue of memory for so large number of iterations

#function Partial_Inf_cpl : a number of infection steps from Step_Inf_cpl, 
#the data is not recorded between steps (direct triming)

###INPUT
##N_it : the number of iterations

##is_I1 : the boolean indicator of size NI for individuals beeing I at the initiation step
#first marginal
##is_I2 : the boolean indicator of size NI for individuals beeing I at the initiation step
#second marginal

##tO : the current time at the start of the function


##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

##Svp : rate for total occurrence of events
    
##vp : the renormalised probabilities for indicating the event

###OUTPUT
#Time : the timing at the end of the N_it steps, starting at tO

##is_If1 :  the boolean indicator of size NI for individuals beeing I at the end
#first marginal
##is_If1 :  the boolean indicator of size NI for individuals beeing I at the end
#second marginal

#%%
def Partial_Inf_cpl(N_it, tO, is_I1, is_I2, AMI, gamma, Svp, vp):
    is_If1 = np.copy(is_I1)
    is_If2 = np.copy(is_I2)
    #sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0
    Time = tO + np.sum(T_exp)/Svp
    rng = np.random.default_rng()
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)    
    for k in np.arange(1, N_it):
        is_If1, is_If2 = Step_Inf_cpl(is_If1, is_If2, Event[k-1], AMI, gamma)
    return Time, is_If1, is_If2

#%%
#function Jumps_Inf_cpl : a number of infection steps from Step_inf, 
#where jumps correspond to the period where no data is kept on the process
#the code is here still nearly exact, only the cost of storage is partly lifted

###INPUT
##t_f>1 : expected final time (initiation at time 1.)
#the actual one will be an approximation expressed through the number of iterations

##t_J : expected time of jump
#the actual one will be an approximation expressed through the number of iterations

##is_I1 : the boolean indicator of size NI for individuals beeing I at the initiation step, first marginal
##is_I2 : the boolean indicator of size NI for individuals beeing I at the initiation step, second marginal


##AMI : Adjacency matrix with intensities
#dataframe of size NEx3, where NE is the number of edges (of order the number of individuals)
#columns of AMI : "Origin", "Destination", "Intensity"
#index set is np.arange(NE)

##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT1 :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step,
#first marginal
##is_IT2 :  the boolean indicator of size (N_it x NI) for individuals beeing I at each step
#second marginal

#%%
def Jumps_Inf_cpl(t_f, t_J, is_I1, is_I2, AMI, gamma):
    #calibration of the duration in terms of a number of iterations
    N_it = 1000
    Time, is_IT1, is_IT2 = Total_Inf_cpl(N_it, is_I1, is_I2, AMI, gamma)
    if not(np.any(is_IT1[-1])):#the infection had stopped previously
        return Time, is_IT1, is_IT2
    else: 
        N_J = int(t_J/(Time[-1]-1)*N_it) 
        N_it2= int((t_f-1) /t_J)+1
        #calibration completed, we go on with the initialisation
        Time = np.zeros(N_it2)
        is_IT1 = np.zeros((N_it2, is_I1.size), dtype = bool)
        is_IT2 = np.zeros((N_it2, is_I2.size), dtype = bool)
        Time[0] = 1.
        is_IT1[0] = np.copy(is_I1)
        is_IT2[0] = np.copy(is_I2)
        #for the choice of the potential events
        vp = np.concatenate((np.repeat(gamma, is_I1.size), AMI.loc[:, "Intensity"]))
        #change of the time scale through the rate of events
        Svp = np.sum(vp)
        vp = vp/Svp
        #iteration procedure of steps without memory
        for k in np.arange(1, N_it2):
            Time[k], is_IT1[k], is_IT2[k]= Partial_Inf_cpl(N_J, Time[k-1], is_IT1[k-1], is_IT2[k-1], AMI, gamma, Svp, vp)
        return Time, is_IT1, is_IT2
    