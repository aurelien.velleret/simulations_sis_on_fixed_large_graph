# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:20:05 2022

@author: Aurélien Velleret
"""


#%%
import numpy as np
import numpy.random as npr
import random as rd
import matplotlib.pyplot as plt
import time 
from mpl_toolkits.mplot3d import Axes3D
import scipy.sparse as sp
import pandas as pd


from sparse_data_selection import summary_parameters
version = summary_parameters.loc[0, "version"]

# summary_per_time  = pd.read_csv("summary_per_time_v{:d}.csv".format(version))
summary_per_pop_size = pd.read_csv("summary_per_pop_size_v{:d}-2.csv".format(version))
# summary_per_indiv = pd.read_csv("summary_per_indiv_v{:d}.csv".format(version))
# AMI = pd.read_csv("AMI_v{:d}.csv".format(version))

#NI =input("How many individuals are to be considered?\n")
NI = summary_parameters.loc[0, "Nbr_indiv_ref"]
uA = summary_parameters.loc[0,'p_init_infected'] 
is_I = npr.choice([False, True], size = NI, p = [1-uA, uA])
gamma = summary_parameters.loc[0,'rate_remission']

#epsN =input("What is the scale of meeting interactions?\n")
#alpha =input("What is the power-law for this scaling?\n")
alpha = summary_parameters.loc[0,'alpha']
epsN = 1/NI**alpha

# #wI0 =input("How likely it is for two connected individuals to transmit an infection?\n")
wI0 = summary_parameters.loc[0,'w_I_ref']
# #wE0 =input("How likely it is for two individuals to be connected?\n")
wE0 = summary_parameters.loc[0,'w_E_ref'] 

t_f = summary_parameters.loc[0,'final_time'] 
t_J = summary_parameters.loc[0,'duration_btw_observations']

#%%
Time = summary_per_time['Current_time']

N_it = Time.size

plt.clf()
plt.plot(Time, summary_per_time["proportion_infected"], label = "proportion of infected")
plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(N_it), label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_v{:d}.png".format(version))
plt.show()



#%%

t_o = summary_parameters.loc[0, 'duration_bfr_start_observations'] 
NIK = np.array(summary_per_pop_size['Nbr_indiv'])
NN = NIK.size

MeanI = np.array(summary_per_pop_size['mean_time_infected'])
StdI = np.array(summary_per_pop_size['std_per_time_of_time_infected'])
MinI = np.array(summary_per_pop_size['min_prop_over_time'])
MaxI = np.array(summary_per_pop_size['max_prop_over_time'])

# MeanIG = np.array(summary_per_pop_size['mean_time_infected_in_giant'])
# StdIG = np.array(summary_per_pop_size['std_per_time_of_time_infected_in_giant'])

plt.clf()
plt.plot(np.log10(NIK), (1-gamma/(wI0*wE0))*np.ones(NN), "k+-", label = "expected equilibrium")
plt.plot(np.log10(NIK), MeanI, "b*--", label = "avg")
plt.plot(np.log10(NIK), MeanI-2*StdI, "vr:")
plt.plot(np.log10(NIK), MeanI+2*StdI, "^r:", label = "2 std")
plt.vlines(np.log10(NIK), \
          ymin = MeanI-2*StdI,\
        ymax = MeanI+2*StdI,  colors = "red")
plt.plot(np.log10(NIK), MinI, "vm:")
plt.plot(np.log10(NIK), MaxI, "^m:", label = "range")
plt.vlines(np.log10(NIK), \
          ymin = MinI,\
        ymax = MaxI,  colors = "m")
plt.ylim(0, 1)
plt.xlabel("Log population size")
plt.ylabel("Equilibrium fluctuations")
plt.legend()
plt.title("Distribution of infected individuals\n \
as a function of population size")
plt.savefig("conc_error_v{:d}_diff_N.png".format(version))
plt.show()

#%%
X = np.log10(NIK)
Y1 = np.log10(StdI)
slope1 = (np.sum(X*Y1)-np.sum(X)*np.sum(Y1)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))

# Y2 = np.log10(StdIG)
# slope2 = (np.sum(X*Y2)-np.sum(X)*np.sum(Y2)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))

plt.clf()
plt.plot(np.log10(NIK), np.log10(StdI), "m*", label = "gnl std")
plt.plot(np.log10(NIK), np.mean(np.log10(StdI)) + slope1*(np.log10(NIK) - np.mean(np.log10(NIK))), "m+--", label = "slope = {:.1f}".format(slope1))
# plt.plot(np.log10(NIK), np.log10(StdIG), "r*", label = "std in giant")
# plt.plot(np.log10(NIK), np.mean(np.log10(StdIG)) + slope2*(np.log10(NIK) - np.mean(np.log10(NIK))), "r+--", label = "slope = {:.1f}".format(slope2))
plt.xlabel("Log population size")
plt.ylabel("Temporal standard deviations")
plt.legend()
plt.title("The effect of population size on the temporal fluctuations\n\
in the numbers of infected")
plt.savefig("std_v{:d}_diff_N_giant.png".format(version))
plt.show()


#%%
X = np.log10(NIK)
Y3 = np.log10(np.abs(MeanI - 1+ gamma/(wI0*wE0)))
slope3 = (np.sum(X*Y3)-np.sum(X)*np.sum(Y3)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))
# Y4 = np.log10(np.abs(MeanIG - 1+ gamma/(wI0*wE0)))
# slope4 = (np.sum(X*Y4)-np.sum(X)*np.sum(Y4)/np.size(X))/ (np.sum(X**2)-np.sum(X)**2/np.size(X))

plt.clf()
plt.plot(np.log10(NIK), Y3, "b*:",  label = "cvg")
plt.plot(np.log10(NIK), np.mean(Y3) + slope3*(X - np.mean(X)), "b+--", label = "slope = {:.2f}".format(slope3))
# plt.plot(np.log10(NIK), Y4, "c*:", label = "cvg in giant")
# plt.plot(np.log10(NIK), np.mean(Y4) + slope4*(X - np.mean(X)), "c+--", label = "slope = {:.2f}".format(slope4))


plt.xlabel("Log population size")
plt.ylabel("Log Discrepancy of the averages")
plt.legend()
plt.title("The effect of population size on the convergence\n\
of the numbers of infected")
plt.savefig("mean_v{:d}_diff_N_giant.png".format(version))
plt.show()

#%%
plt.clf()
plt.plot(np.log10(NIK), MeanIG, "b*--", label = "avg in GC")
plt.plot(np.log10(NIK), MeanIG-2*StdIG, "vr:")
plt.plot(np.log10(NIK), MeanIG+2*StdIG, "^r:")
plt.vlines(np.log10(NIK), \
          ymin = MeanIG-2*StdIG,\
        ymax = MeanIG+2*StdIG,\
        colors = "red", label = "avg@2sig in GC")
plt.plot(np.log10(NIK), MeanI, "c*--", alpha = 0.3, label = "avg")
plt.plot(np.log10(NIK), MeanI-2*StdI,  "mv:", alpha = 0.3)
plt.plot(np.log10(NIK), MeanI+2*StdI, "m^:", alpha = 0.3)
plt.plot(np.log10(NIK), (1-gamma/(wI0*wE0))*np.ones(NN), "k+-", label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Log population size")
plt.ylabel("Equilibrium fluctuations")
plt.legend()
plt.title("Distribution of infected individuals in the giant\n \
as a function of population size")
plt.savefig("conc_error_v{:d}_diff_N_giant.png".format(version))
plt.show()

#%%
I_ev = (Time>t_o)


Mean_indiv = summary_per_indiv['Proportion_time_infected'] 
Sd_indiv = summary_per_indiv['std_time_infected'] 
In_giant = summary_per_indiv['Is_in_giant']

plt.clf()
plt.hist(Mean_indiv[Mean_indiv>0], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv[Mean_indiv>0]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individual infected in the long run")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend()
plt.savefig("Mean_time_inf_v{:d}.png".format(version))
plt.show()

#%%
I_ev = (Time>t_o)

plt.clf()
plt.hist(Mean_indiv[In_giant], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv[In_giant]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individuals in the giant component")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend()
plt.savefig("Mean_time_inf_giant_v{:d}.png".format(version))
plt.show()

#%%

iD, degree_C = np.unique(AMI[["Origin"]], return_counts =True)
degree = np.zeros(Mean_indiv.size)
degree[iD] = degree_C

plt.clf()
plt.hist(degree)
plt.title("Degree distribution")
#plt.xlabel("degree")
plt.savefig("degree_distribution_v{:d}.png".format(version))
plt.show()
#%%
plt.clf()
plt.plot(degree[Mean_indiv>0], Mean_indiv[Mean_indiv>0], 'r+')
plt.ylabel("mean time being infected")
plt.ylabel("individual degree")

#%%
deg_m = np.min(degree[In_giant])-0.5
deg_M = np.max(degree[In_giant])+0.5
L = int(min(deg_M-deg_m+1, 50))

plt.clf()
plt.hist2d(degree[Mean_indiv>0], Mean_indiv[Mean_indiv>0], \
         bins = [np.linspace(deg_m, deg_M, L), np.linspace(0.5, 1, 30)], density = True)
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")
plt.colorbar()
plt.savefig("Time_inf_vs_degree_v{:d}.png".format(version))
plt.show()
#%%
deg_av = np.mean(degree[In_giant])
deg_std = np.std(degree[In_giant])
kwargs = dict(histtype='stepfilled', alpha=0.4, density=True, bins = np.linspace(0.4, 1, 20), ec="k")
degT = np.array([deg_av-2*deg_std, deg_av, deg_av+2*deg_std], dtype = int)
Nv = degT.size

plt.clf()
for i in np.arange(Nv):    
    plt.hist(Mean_indiv[degree == degT[i]],**kwargs, fc=(i/Nv, 1-i/Nv, 1-i/Nv, 0.3) , label = "degree {:d}".format(degT[i]))
plt.legend(loc ="upper left")
plt.title("Infection level as a function of the degree")
plt.xlabel("mean time being infected")
plt.ylabel("normalized density for each degree")
plt.savefig("Time_inf_by_degree_v{:d}-1.png".format(version))
plt.show()
#%%

