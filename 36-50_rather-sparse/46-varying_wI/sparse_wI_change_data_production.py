# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:20:05 2022

@author: Aurélien Velleret
"""


#%% Function extraction and parameter setting
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import time
import pandas as pd


from sparse_functions import AMI_Def_1, Step_Inf, Total_Inf, Partial_Inf, Jumps_Inf, Graph_connect
from sparse_data_selection import summary_parameters, summary_per_wI
version = summary_parameters.loc[0, "version"]

#%% Definition of the main parameters
#NI =input("How many individuals are to be considered?\n")
NI = summary_parameters.loc[0, "Nbr_indiv_ref"]
uA = summary_parameters.loc[0, 'p_init_infected']
is_I = npr.choice([False, True], size=NI, p=[1-uA, uA])
gamma = summary_parameters.loc[0, 'rate_remission']

#epsN =input("What is the scale of meeting interactions?\n")
#alpha =input("What is the power-law for this scaling?\n")
alpha = summary_parameters.loc[0, 'alpha']
epsN = 1/NI**alpha

# #wI0 =input("How likely it is for two connected individuals to transmit an infection?\n")
wI0 = summary_parameters.loc[0, 'w_I_ref']
# #wE0 =input("How likely it is for two individuals to be connected?\n")
wE0 = summary_parameters.loc[0, 'w_E_ref']

t_f = summary_parameters.loc[0, 'final_time']
t_J = summary_parameters.loc[0, 'duration_btw_observations']
#%% Control study with the other simulations
AMI = AMI_Def_1(NI, epsN, wE0,  wI0)

NC, CoCom = Graph_connect(AMI, NI)
print("Number of connected components : {:d}".format(NC))
summary_parameters['Conn_Comp_ref'] = NC

A, CodCom = np.unique(CoCom, return_counts=True)
ordj = np.argsort(CodCom)
print("Size of the giant connected component : {:d}".format(CodCom[ordj[-1]]))
summary_parameters['Size_giant_Comp'] = CodCom[ordj[-1]]
# In this case, except the main component, only 19 isolated individuals are added


start_time = time.process_time()
Time, is_IT = Jumps_Inf(t_f, t_J, is_I, AMI, gamma)
time_exec = time.process_time() - start_time
# %% Plot dist_inf
summary_per_time = pd.DataFrame(Time, columns=['Current_time'])
summary_per_time["proportion_infected"] = np.sum(is_IT, axis=1)/is_IT.shape[1]
summary_per_time.to_csv("summary_per_time_v{:d}.csv".format(version))


N_it = Time.size
plt.clf()
plt.plot(Time, summary_per_time["proportion_infected"],
         label="proportion of infected")
plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']
         * np.ones(N_it), label="expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_v{:d}.png".format(version))
plt.show()

#%% Setting for the variation in wI

t_o = summary_parameters.loc[0, 'duration_bfr_start_observations']
wIK = np.array(summary_per_wI['Transmission_rate'])
NwI = wIK.size

#%% First observation
plt.clf()
wEn = r'w^{(n)}_E'
wIn = r'w^{(n)}_I'

plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(Time.size),
         label="equilibrium $u_\star$")
for k in np.arange(0, NwI, int(NwI/6)):
    # the function int is called to removed the integer being int32
    # because AMI exploits NI*NI in possibly larger format
    wIk = wIK[k]
    AMI = AMI_Def_1(NI, epsN/NI**alpha, wE0*wI0/wIk,  wIk)
    is_I = npr.choice([False, True], size=NI, p=[1-uA, uA])
    Time, is_IT = Jumps_Inf(t_f, t_J, is_I, AMI, gamma)
    plt.plot(Time, np.sum(is_IT, axis=1)/is_IT.shape[1],
             label="${:s}$ = {:.2f},\
$n\, {:s}$ = {:.1f}".format(wIn, wIk, wEn, epsN/NI**alpha * wE0*wI0/wIk))


plt.ylim(0, 1)
plt.title("Dynamics of the proportion of infected individuals\n\
for increasing values of ${:s}$".format(wIn))
plt.xlabel("Time")
plt.ylabel("Proportion of infected individuals")
plt.legend(loc = "lower left", title = "${:s}$ fixed, varying ${:s}$".format(r"n=n_0", wIn), \
            prop={'size': 10})
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_{:d}_diff_wI.png".format(version))
plt.show()

#%% Definition of the data arrays

MeanI = np.zeros(NwI)
StdI = np.zeros(NwI)
MinI = np.zeros(NwI)
MaxI = np.zeros(NwI)
MeanIG = np.zeros(NwI)
StdIG = np.zeros(NwI)
# %%

plt.clf()

for k in np.arange(NwI):
    # the function int is called to removed the integer being int32
    # because AMI exploits NI*NI in possibly larger format
    wIk = wIK[k]
    AMI = AMI_Def_1(NI, epsN/NI**alpha, wE0*wI0/wIk,  wIk)
    is_I = npr.choice([False, True], size=NI, p=[1-uA, uA])
    Time, is_IT = Jumps_Inf(t_f, t_J, is_I, AMI, gamma)
    MeanI[k] = np.mean(is_IT[int(t_o/t_J):])
    summary_per_wI.loc[k, 'mean_time_infected'] = MeanI[k]
    StdI[k] = np.std(np.mean(is_IT[int(t_o/t_J):], axis=1))
    summary_per_wI.loc[k, 'std_per_time_of_time_infected'] = StdI[k]
    MinI[k] = np.min(np.mean(is_IT[int(t_o/t_J):], axis=1))
    summary_per_wI.loc[k, 'min_prop_over_time'] = MinI[k]
    MaxI[k] = np.max(np.mean(is_IT[int(t_o/t_J):], axis=1))
    summary_per_wI.loc[k, 'max_prop_over_time'] = MaxI[k]
    summary_per_wI.to_csv('summary_per_wI_v{:d}.csv'.format(version))

    NC, CoCom = Graph_connect(AMI, NI)
    print("Number of connected components at wI {:.3e}: {:d}".format(wIk, NC))
    summary_per_wI.loc[k, 'Nbr_conn_comp'] = NC
    A, CodCom = np.unique(CoCom, return_counts=True)

    ID_giant = np.argsort(CodCom)[-1]
    In_giant = (CoCom == A[ID_giant])
    summary_per_wI.loc[k, 'Size_conn_comp'] = np.sum(In_giant)
    #
    MeanIG[k] = np.mean(is_IT[int(t_o/t_J):][:, In_giant])
    summary_per_wI.loc[k, 'mean_time_infected_in_giant'] = MeanIG[k]
    StdIG[k] = np.std(np.mean(is_IT[int(t_o/t_J):][:, In_giant], axis=1))
    summary_per_wI.loc[k, 'std_per_time_of_time_infected_in_giant'] = StdIG[k]
    summary_per_wI.to_csv('summary_per_wI_v{:d}.csv'.format(version))
    plt.plot(Time, np.sum(is_IT, axis=1)/is_IT.shape[1],
             label="wI = {:.2f}".format(wIk))


plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(Time.size),
         label="expected equilibrium")
plt.ylim(0, 1)
plt.title("Dynamics of the proportion of infected individuals\n\
for increasing values of wI at a fixed kernel value")
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_{:d}_diff_wI.png".format(version))
plt.show()


# AMI.to_csv("AMI_v{:d}.csv".format(version))

#%% Data recollection
summary_per_wI = pd.read_csv('summary_per_wI_v{:d}.csv'.format(version), index_col = 0)
wIK = np.array(summary_per_wI["Transmission_rate"])
NwI = wIK.size
MeanI = np.array(summary_per_wI['mean_time_infected'] )
StdI = np.array(summary_per_wI['std_per_time_of_time_infected']) 
MinI = np.array(summary_per_wI['min_prop_over_time'])
MaxI = np.array(summary_per_wI['max_prop_over_time'])
MeanIG =  np.array(summary_per_wI['mean_time_infected_in_giant'])
StdIG = np.array(summary_per_wI['std_per_time_of_time_infected_in_giant'] )

#%% Plot conc_error_v{:d}_diff_wI
fig = plt.figure()
plt.clf()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()


ax1.plot(wIK, (1-gamma/(wI0*wE0))*np.ones(NwI),
         "k+-", label="expected equilibrium")
ax1.plot(wIK, MeanI, "b*--", label="avg")
ax1.plot(wIK, MeanI-2*StdI, "vr:")
ax1.plot(wIK, MeanI+2*StdI, "^r:", label="2 std")
ax1.vlines(wIK,
           ymin=MeanI-2*StdI,
           ymax=MeanI+2*StdI,  colors="red")
ax1.plot(wIK, MinI, "vm:")
ax1.plot(wIK, MaxI, "^m:", label="range")
ax1.vlines(wIK,
           ymin=MinI,
           ymax=MaxI,  colors="m")

ax1.set_xlabel(r"Transmission rate $w_I^{(n_0)}$")

new_tick_locations = np.array([wIK[0], .5, 1., 1.5, 2., 2.5])
def tick_function(X):
    V = wI0*wE0/X
    return ["%.1f" % z for z in V]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel(r"Mean contact number: $n_0 w_E^{(n_0)}$")


plt.ylim(0, 1)
plt.ylabel("Equilibrium fluctuations")
ax1.legend()
plt.title("Distribution of infected individuals\n \
as a function of the transmission rate")
plt.savefig("conc_error_v{:d}_diff_wI.png".format(version))
plt.show()

# %% Plot std_v{:d}_diff_wI_giant
X1 = wIK
Y1 = StdI
slope1 = (np.sum(X1*Y1)-np.mean(X1)*np.sum(Y1)) / (np.size(X1) * np.var(X1))
summary_parameters["slope_std_with_wI"] = slope1
X2 = wIK
Y2 = StdIG
slope2 = (np.sum(X2*Y2)-np.mean(X2)*np.sum(Y2)) / (np.size(X2) * np.var(X2))
summary_parameters["slope_std_in_giant_with_wI"] = slope2

fig = plt.figure()
plt.clf()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()

ax1.plot(X1, Y1, "m*", label="gnl std")
ax1.plot(X1, np.mean(Y1) + slope1*(X1 - np.mean(X1)),
         "m+--", label="slope = {:.2f}".format(slope1))
ax1.plot(wIK, Y2, "r*", label="std in giant")
ax1.plot(X2, np.mean(Y2) + slope2*(X2 - np.mean(X2)),
         "r+--", label="slope = {:.2f}".format(slope2))

ax1.set_xlabel(r"Transmission rate $w_I^{(n_0)}$")

new_tick_locations = np.array([wIK[0], .5, 1., 1.5, 2., 2.5])
def tick_function(X):
    V = wI0*wE0/X
    return ["%.1f" % z for z in V]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel(r"Mean contact number: $n_0 w_E^{(n_0)}$")


ax1.legend()
plt.ylabel("Temporal standard deviations")
plt.title("The effect of the transmission rate on the temporal fluctuations\n\
in the numbers of infected")
plt.savefig("std_v{:d}_diff_wI_giant.png".format(version))
plt.show()

#%% Plot mean_v{:d}_diff_wI_giant


X3 = wIK
Y3 = np.abs(MeanI - 1 + gamma/(wI0*wE0))
slope3 = (np.sum(X3*Y3)-np.mean(X3)*np.sum(Y3)) / (np.size(X3) * np.var(X3))
summary_parameters["slope_prop_error_with_wI"] = slope3
X4 = wIK
Y4 = np.abs(MeanIG - 1 + gamma/(wI0*wE0))
slope4 = (np.sum(X4*Y4)-np.mean(X4)*np.sum(Y4)) / (np.size(X4) * np.var(X4))
summary_parameters["slope_prop_error_in_giant_with_wI"] = slope4


fig = plt.figure()
plt.clf()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()

ax1.plot(X3, Y3, "b*:",  label="cvg")
ax1.plot(X3, np.mean(Y3) + slope3*(X3 - np.mean(X3)),
         "b+--", label="slope = {:.2f}".format(slope3))
ax1.plot(X4, Y4, "c*:", label="cvg in giant")
ax1.plot(X4, np.mean(Y4) + slope4*(X4 - np.mean(X4)),
         "c+--", label="slope = {:.2f}".format(slope4))
ax1.plot(X3, np.mean(Y3) - alpha*(X3 - np.mean(X3)), "k+--",
         label="predicted slope = {:.2f}".format(-alpha))

ax1.set_xlabel(r"Transmission rate $w_I^{(n_0)}$")

new_tick_locations = np.array([wIK[0], .5, 1., 1.5, 2., 2.5])

def tick_function(X):
    V = wI0*wE0/X
    return ["%.1f" % z for z in V]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel(r"Mean contact number: $n_0 w_E^{(n_0)}$")

ax1.set_ylabel("Discrepancy of the averages")
ax1.legend()
plt.title("The effect of the transmission rate on the convergence\n\
of the numbers of infected")
plt.savefig("mean_v{:d}_diff_wI_giant.png".format(version))
plt.show()

# %% Plot conc_error_v{:d}_diff_wI_giant
fig = plt.figure()
plt.clf()
ax1 = fig.add_subplot(111)
ax2 = ax1.twiny()


ax1.plot(wIK, (1-gamma/(wI0*wE0))*np.ones(wIK.size),
         "k+-", label = "{:s}: expected equilibrium".format(r'$u_*$'))
ax1.plot(wIK, MeanIG, "rx--", label = "{:s} (in the giant)".format(r'$\hat v^{(n)}_*$'))
# ax1.plot(wIK, MeanIG-2*StdIG, "vr:", alpha = 0.3)
# ax1.plot(wIK, MeanIG+2*StdIG, "^r:", alpha = 0.3)
ax1.vlines(wIK, \
          ymin = MeanIG-2*StdIG,\
        ymax = MeanIG+2*StdIG, alpha = 0.3,\
        colors = "red")
        #, label = "{:s} in the giant".format(r'$\hat{u}^{(n)}_* \pm 2\, \sigma^{(n)}$'))
ax1.plot(wIK, MeanI, "mx--", label = "{:s} (overall)".format(r'$\hat{u}^{(n)}_*$'))
ax1.vlines(wIK, \
          ymin = MeanI-2*StdI,\
        ymax = MeanI+2*StdI, alpha = 0.3,\
        colors = "m")
    #, label = "overall {:s}".format(r'$\hat{u}^{(n)}_* \pm 2\, \sigma^{(n)}$'))
# ax1.plot(wIK, MeanI-2*StdI,  "mv:", alpha = 0.3)
# ax1.plot(wIK, MeanI+2*StdI, "m^:")
ax1.set_xlabel(r"Transmission rate $w_I^{(n_0)}$")
ax1.legend()

new_tick_locations = np.array([wIK[0], .5, 1., 1.5, 2., 2.5])

def tick_function(X):
    V = wI0*wE0/X
    return ["%.1f" % z for z in V]

ax2.set_xlim(ax1.get_xlim())
ax2.set_xticks(new_tick_locations)
ax2.set_xticklabels(tick_function(new_tick_locations))
ax2.set_xlabel(r"Mean contact number: $n_0 w_E^{(n_0)}$")

plt.ylim(0, 1)
ax1.set_ylabel("Proportion of infected individuals")
plt.title("Asymptotic proportion of infected individuals\n \
as a function of {:s}, the transmission rate".format(r'$w^{(n_0)}_I$'))
plt.savefig("conc_error_v{:d}_diff_wI_giant.png".format(version),\
            bbox_inches="tight")
plt.show()

#%% Plot Mean_time_inf
I_ev = (Time > t_o)

Mean_indiv = np.mean(is_IT[I_ev], axis=0)
Sd_indiv = np.std(is_IT[I_ev], axis=0)
In_giant = (Mean_indiv > 0)
summary_per_indiv = pd.DataFrame(
    Mean_indiv, columns=['Proportion_time_infected'])
summary_per_indiv['std_time_infected'] = Sd_indiv
summary_per_indiv['Is_in_giant'] = In_giant

plt.clf()
plt.hist(Mean_indiv[Mean_indiv > 0], bins=np.linspace(
    0, 1, 30), fc=(0, 1, 1, 0.5),  density=True)
plt.axvline(x=np.mean(Mean_indiv[Mean_indiv > 0]),
            color="b", label="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color="r", label="expected mean")
plt.title("Distribution of mean time being infected\n \
for individual infected in the long run")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend()
plt.savefig("Mean_time_inf_v{:d}.png".format(version))
plt.show()

summary_per_indiv.to_csv('summary_per_indiv_v{:d}.csv'.format(version))

#%% Plot Mean_time_inf_giant
plt.clf()
plt.hist(Mean_indiv[In_giant], bins=np.linspace(
    0, 1, 30), fc=(0, 1, 1, 0.5),  density=True)
plt.axvline(x=np.mean(Mean_indiv[In_giant]), color="b", label="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color="r", label="expected mean")
plt.title("Distribution of mean time being infected\n \
for individuals in the giant component")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend()
plt.savefig("Mean_time_inf_giant_v{:d}.png".format(version))
plt.show()

# %% Plot degree_distribution

iD, degree_C = np.unique(AMI[["Origin"]], return_counts=True)
degree = np.zeros(Mean_indiv.size)
degree[iD] = degree_C

plt.clf()
plt.hist(degree, range=(0, np.max(degree)), bins=int(np.max(degree)))
plt.title("Degree distribution")
# plt.xlabel("degree")
plt.savefig("degree_distribution_v{:d}.png".format(version))
plt.show()
# %% Plot 2 degree_distribution
plt.clf()
values, counts = np.unique(degree, return_counts=True)
plt.bar(values, counts/np.sum(counts))
plt.title("Degree distribution\n\
wI = {:.2f}".format(wIk))
plt.xlabel('degree')
plt.ylabel("proportion of vertices")
plt.savefig("degree_distribution_v{:d}.png".format(version))
plt.show()

# %% Plot Infection level as a function of the degree
plt.clf()
plt.plot(degree[Mean_indiv > 0], Mean_indiv[Mean_indiv > 0], 'r+')
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")

# %% Plot Time_inf_vs_degree
deg_m = np.min(degree[In_giant])-0.5
deg_M = np.max(degree[In_giant])+0.5
L = int(min(deg_M-deg_m+1, 50))

plt.clf()
plt.hist2d(degree[Mean_indiv > 0], Mean_indiv[Mean_indiv > 0],
           bins=[np.linspace(deg_m, deg_M, L), np.linspace(0.5, 1, 30)], density=True)
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")
plt.colorbar()
plt.savefig("Time_inf_vs_degree_v{:d}.png".format(version))
plt.show()
# %% Plot Time_inf_by_degree_v{:d}-1
deg_av = np.mean(degree[In_giant])
deg_std = np.std(degree[In_giant])
kwargs = dict(histtype='stepfilled', alpha=0.4, density=True,
              bins=np.linspace(0.4, 1, 20), ec="k")
degT = np.array([deg_av-2*deg_std, deg_av, deg_av+2*deg_std], dtype=int)
Nv = degT.size

plt.clf()
for i in np.arange(Nv):
    plt.hist(Mean_indiv[degree == degT[i]], **kwargs,
             fc=(i/Nv, 1-i/Nv, 1-i/Nv, 0.3), label="degree {:d}".format(degT[i]))
plt.legend(loc="upper left")
plt.title("Infection level as a function of the degree")
plt.xlabel("mean time being infected")
plt.ylabel("normalized density for each degree")
plt.savefig("Time_inf_by_degree_v{:d}-1.png".format(version))
plt.show()


#%%