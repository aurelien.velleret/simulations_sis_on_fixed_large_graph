# -*- coding: utf-8 -*-
"""
Created on Sat Oct  1 19:37:22 2022

@author: alter
"""

#%%
import numpy as np
import numpy.random as npr
import pandas as pd
import scipy.sparse as sp

#%%

#In the mean-field situation whith identical individuals, 
#we only need to keep track of the number 
#of S vs I individuals
#%%

#function Total_inf_MG : a number of infection steps from Step_inf_cpl

###INPUT
## N_it : the number of iterations

## N_I0 : the intial number of individuals being I at this step,
#for the association, N_I0 = np.sum(is_I) close to NI*uA

##NI : the total population size

## w : global infection rate, should correspond to wI0*wE0

## gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##N_Inf : number of infected individuals at each step, 
#of size N_it 


#%%

def Total_Inf_MG(N_it, N_I0, NI, gamma, w):
    #the Uniform variables exploited for the choice of the events
    Event = npr.rand(N_it-1)
    #setting of the timing through exponential random times
    T_exp = npr.exponential(size = N_it-1)
    #initialization
    Time = np.ones(N_it)
    N_Inf = np.zeros(N_it, dtype = int)
    N_Inf[0] = N_I0
    k= 1
    while (k<N_it)&(N_Inf[k-1]>0):
        rate = N_Inf[k-1]*(gamma + w * (1- N_Inf[k-1]/NI))
        Time[k] = Time[k-1] + T_exp[k-1]/rate
        if Event[k-1] < gamma * N_Inf[k-1] / rate:
            #it is a remission event
            N_Inf[k] = N_Inf[k-1] -1
        else:
            #it is an infection event
            N_Inf[k] = N_Inf[k-1] + 1
        k = k+1
    return Time, N_Inf

#%%
#function  Partial_Inf_cpl: a number of infection steps from Step_inf_cpl 
#without memory

#%%

def Partial_Inf_MG(N_it, N_I1, T1, NI, gamma, w):
    #the Uniform variables exploited for the choice of the events
    Event = npr.rand(N_it-1)
    #setting of the timing through exponential random times
    T_exp = npr.exponential(size = N_it-1)
    #initialization
    T2 = T1
    N_I2 = N_I1
    k= 1
    while (k<N_it)&(N_I2>0):
        rate = N_I2*(gamma + w * (1- N_I2/NI))
        T2 = T2 + T_exp[k-1]/rate
        if Event[k-1] < gamma * N_I2 / rate:
            #it is a remission event
            N_I2 = N_I2 -1
        else:
            #it is an infection event
            N_I2 = N_I2 + 1
        k = k+1
    return T2, N_I2

#%%
#function Jumps_Inf_MG : a number of infection steps from Step_inf_cpl, 
#where jumps correspond to the period where no data is kept on the process
#the code is here still nearly exact, only the cost of storage is partly lifted

###INPUT
##t_f>1 : expected final time (initiation at time 1.)
#the actual one will be an approximation expressed through the number of iterations

##t_J : expected time of jump
#the actual one will be an approximation expressed through the number of iterations

## N_I0 : the intial number of individuals being I at this step,
#for the association, N_I0 = np.sum(is_I) close to NI*uA

##NI : the total population size

## w : global infection rate, should correspond to wI0*wE0

## gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##N_Inf : number of infected individuals at each step, 
#of size N_it 

#%%
def Jumps_Inf_MG(t_f, t_J, N_I0, NI, gamma, w):
    #calibration of the duration in terms of a number of iterations
    N_it = 1000
    Time, N_inf = Total_Inf_MG(N_it, N_I0, NI, gamma, w)
    if N_inf[-1] == 0:#the infection had stopped previously
        Nf = np.sum(Time >0)
        print("end of the infection at time {:.2f}".format(Time[Nf-1]))
        return Time[:Nf], N_inf[:Nf]
    else: 
        N_J = int(t_J/(Time[-1]-1)*N_it) 
        N_it2= int((t_f-1) /t_J)+1
        #calibration completed, we go on with the initialisation
        Time = np.zeros(N_it2)
        N_inf2 = np.zeros(N_it2, dtype = int)
        k = 1
        Time[0] = 1.
        N_inf2[0] = N_I0
        #iteration procedure of steps without memory
        k = 1
        while (k < N_it2)&(N_inf2[k-1]>0):
            Time[k], N_inf2[k] = Partial_Inf_MG(N_J, N_inf2[k-1], Time[k-1], NI, gamma, w)
            k = k+1
        Nf = np.sum(Time >0)
        return Time[:Nf], N_inf2[:Nf]
 #%%
