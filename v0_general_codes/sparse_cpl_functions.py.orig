# -*- coding: utf-8 -*-
"""
Created on Sat Oct  1 19:37:22 2022

@author: alter
"""

#%%
import numpy as np
import numpy.random as npr
import pandas as pd
import scipy.sparse as sp

#%%

#We aim at an efficient coupling between the fixed graph and mean-field process
#along the lines presented in the proof. We focus on the singloton case, so that 
#sparse matrix formulation can be easily exploited.


#%%
#function AMI_Def_coo_1D : construction of the sparse Adjacency matrix, in 

def AMI_Def_coo_1D(NI, epsN, w_I, w_E):
    AMI1 = sp.random(NI, NI, density=w_E/(NI*epsN), data_rvs=np.ones)
    #symmetrisation of the coordinates + entry values
    I_triang_sup = (AMI1.row<AMI1.col)
    U1 = np.array(AMI1.row[I_triang_sup], dtype = int)
    U2 = np.array(AMI1.col[I_triang_sup], dtype = int)
    row = np.concatenate((U1, U2))
    col = np.concatenate((U2, U1))
    AMI = sp.coo_matrix((np.repeat(w_I*epsN, row.size), (row, col)), shape=(NI, NI))
    return AMI

#%%


#function Graph_Def_hom : construction of the edge ordering, including AMI, 
#where individuals are not distinguished

###INPUT
##NI: number of individuals

#Number of doubly activated edges to be considered

##espN: indicator of sparsity for the graph with respect to NI

##w_I0: contribution to w for the intensity of activation along present edges

##w_E0: contribution to w for the probability to keep an edge

###OUTPUT
##AMI : Adjacency matrix in sparse coo format,
#the crucial information will be decomposed once and for all selection purposes
# (rows : "Origin"), (columns : "Destination"), (keys : conjonction for rapid identification)

##MGE: "Mixed Graph Edges" ordered edges in the graph excluding FG


##I cannot order all the edges because of memory errors occuring 
#already for 1E5 individuals...
#Thus I need to anticipate for the number of edges that could be activated twice in the interval
#This could serve as the upper-time limit
#%%
def Graph_Def_hom(NI, epsN, w_I0, w_E0):
    w_EN = w_E0/(NI*epsN)
    w_IN = w_I0*epsN
    
	#index for each of the edge
    EO = npr(NI**2)
	#only the triangular data is kept
    EO = EO[EO//NI < EO%NI]
    npr.shuffle(EO)
	#number of edges belonging to AMI
    N_AL = npr.binomial(EO.size, w_EN)
    col = np.concatenate((EO[:N_AL]%NI, EO[:N_AL]//NI))
    row = np.concatenate((EO[:N_AL]//NI, EO[:N_AL]%NI))
    AMI = sp.coo_matrix((np.repeat(w_IN, 2*N_AL), (col, row)), shape=(NI, NI))
    MGE = np.vstack((EO[N_AL:]//NI, EO[N_AL:]%NI))
    return AMI, MGE

#%%

#function Timing_cpl: adjustment of the timing and succession of events

###INPUT
##t_f>1 : expected final time (initiation at time 1.)
#the actual one will be an approximation obtained by truncation

##AMI : Adjacency matrix in sparse coo format,
#the crucial information will be decomposed once and for all selection purposes
# (rows : "Origin"), (columns : "Destination"), (keys : conjonction for rapid identification)

##MGE: array of size 2xN_M specifying the edges outside of FG in random order

##gamma : remission rate (constant)

##w_IN: intensity of activation along present edges

#w_EN: probability to keep an edge

###OUTPUT

##Time: array of time, size N_ev

##Event: index that specifies the event to be considered:
#"Rem": remission
#"FG_Inf": infection along the fixed graph
#"MG_Inf": infection along another edge previously activated
#"Act_MG": activation of an additional edge outside of the fixed graph (only their number is kept)

##Order: array of integers of size N_ev that specifies the entry corresponding to the event 
#in the following arrays for the first 3 events, the current number of unactivated edges for the last one

##I_Rem: array of random size specifying the index of the individuals being remitted (in [1, N_I])

##I_FG: array of random size 2xN_c specifying the index of the individuals for "FG_Inf"
#being potential Origin (first component: 0)
#and potentially infected individual (first component: 1)

##I_MG: array of random size 2xN_c specifying the index of the individuals for "MG_Inf"
#being potential Origin (first component: 0)
#and potentially infected individual  (first component: 1)

#%%
def Timing_cpl():
	


	return Time, Event, Order, I_Rem, I_FG, I_MG

#%%

#function Step_cpl_FG_Inf : one step of iteration when the event is an infection along the fixed graph

###INPUT
##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##active_edges: the dok-sparse matrix for edges in AMI that have been previously activated 
#(the reported direction being random, the entry without interest)

##i_Inf: index of the potentially infected individual

##i_Ori: index of the potential origin of this infection

##w_OK : boolean indicator that {v < w_EN} is satisfied


###OUTPUT
##is_I2 : the boolean indicator of size 2xNI for individuals being I at  the end of the step,
#first for the fixed graph model, then for the mean-field

##active_edges: the keys for edges in AMI that have been activated until the end of the step

#%%

def Step_cpl_FG_Inf(is_I, active_edges, i_Inf, i_Ori, w_OK):
	is_I2 = np.copy(is_I)       
    if is_I[0, i_Ori] == True:
            #new set of infected individuals
            is_I[0, i_Inf] = True
    if ((i_Inf, i_Ori) in active_edges)|((i_Ori, i_Inf) in active_edges.keys()):
		#the edge has already been activated, needs to be restarted   
		if w_OK:
			if is_I[1, i_Ori] == True:
				is_I[1, i_Inf] = True
	else:
		#it is the first activation of the edge, so needs to be reported
		active_edges[i_Inf, i_Ori]= 1
		#the infection follows the same rule as for the FG model
		if is_I[1, i_Ori] == True:
                is_I[1, i_Inf] = True
	return is_I, active_edges
 #%%

###function Step_cpl_MG_Inf : one step of iteration when the event is an infection 
##along another edge previously activated
#-> to be directly integrated into the code

###INPUT
##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##i_Inf: index of the potentially infected individual

##i_Ori: index of the potential origin of this infection

##w_OK : boolean indicator that {v < w_EN} is satisfied

###OUTPUT
##is_I : the boolean indicator of size 2xNI for individuals being I at  the end of the step,
#first for the fixed graph model, then for the mean-field

#%%

def Step_cpl_EInf(is_I, i_Inf, i_Ori, w_OK):
	if (is_I[1, i_Ori] == True)&w_OK:
		is_I[1, i_Inf] = True
	return is_I
 #%%

#function Step_cpl_EInf : one step of iteration when the event is a remission -> to be directly included

###INPUT
##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##i_Rem: index of the potentially remitted individual

###OUTPUT
##is_I : the boolean indicator of size 2xNI for individuals being I at  the end of the step,
#first for the fixed graph model, then for the mean-field


#%%  
def Step_cpl_ERem(is_I, i_Rem):
    is_I[:, i_Rem] = False
    return is_I
#%%

#function Total_inf_cpl : a number of infection steps from Step_inf_cpl

###INPUT
##N_it : the number of iterations

##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##AMI : Adjacency matrix in sparse coo format,
#the crucial information will be decomposed once and for all selection purposes
# (rows : "Origin"), (columns : "Destination"), (keys : conjonction for rapid identification)

##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT : the boolean indicator of size (N_it x 2 x NI) for individuals being I 
#at each time step
#first for the fixed graph model, then for the mean-field
#specified by individual

##active_edges : matrix of activated edges in sparse coo format
#(columns : "Destination")  vs (rows : "Origin")


###The procedure goes on even without any infected individual,
### rapidly because the conditions of change are never fulfilled
#%%

def Total_Inf_cpl(N_it, is_I, AMI, gamma, epsN, wI0, wE0):
    NI = is_I.shape[1]
    #choice of the potential events
    vp = np.concatenate((np.repeat(gamma, NI), AMI.data, NI * wI0 * wE0*np.ones(1)))
    #change of the time scale through the rate of events
    Svp = np.sum(vp)
    vp = vp/Svp
    rng = npr.default_rng(12675)
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)
    #setting of the timing
    ##through  the sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0.
    Time = 1 + np.cumsum(T_exp)/Svp
    #initialization
    is_IT = np.zeros((N_it, 2, NI), dtype = bool)
    is_IT[0] = is_I
    NG = AMI.count_nonzero()
    AMI_row = AMI.row
    AMI_col = AMI.col
    AMI_keys = AMI.todok().keys()
    active_edges = sp.dok_matrix((NI, NI), dtype = bool)
    w_EN = wE0/(NI*epsN)
    for k in np.arange(1, N_it):
        is_IT[k], active_edges = Step_Inf_cpl(is_IT[k-1], active_edges, Event[k-1], AMI_row, AMI_col, AMI_keys, NG, w_EN)
    return Time, is_IT

#%%
#issue of memory for so large number of iterations

#function Partial_Inf_cpl : a number of infection steps from Step_Inf_cpl, 
#the data is not recorded between steps (direct triming)

###INPUT
##N_it : the number of iterations

##tO : the current time at the start of the function

##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##active_edges : matrix of activated edges in sparse coo format
#(columns : "Destination")  vs (rows : "Origin")

##AMI : Adjacency matrix in sparse coo format,
#the crucial information is decomposed once and for all selection purposes
# (rows : "Origin"), (columns : "Destination"), (keys : conjonction for rapid identification)

##NG : number of non-zero entries of AMI : NG = AMI.count_nonzero()

##w_EN : probability to keep the edge in the mean-field after activation by wI,
#w_EN = wE0/(NI*epsN)

##Svp : rate for total occurrence of events
    
##vp : the renormalised probabilities for indicating the event

###OUTPUT
#Time : the timing at the end of the N_it steps, starting at tO

##is_I2 : the boolean indicator of size 2xNI for individuals being I at  the end of the step,
#first for the fixed graph model, then for the mean-field

##active_edges : matrix of activated edges in sparse coo format
#(columns : "Destination")  vs (rows : "Origin")


#%%
def Partial_Inf_cpl(N_it, tO, is_I, active_edges, AMI_row, AMI_col, AMI_keys, NG, w_EN, Svp, vp):
    is_I2 = np.copy(is_I)
    #sampling of exponential random times
    T_exp = npr.exponential(size = N_it)
    T_exp[0] = 0
    Time = tO + np.sum(T_exp)/Svp
    rng = np.random.default_rng()
    Event = rng.choice(vp.size, size = N_it, replace = True, p = vp)    
    for k in np.arange(1, N_it):
        is_I2, active_edges = Step_Inf_cpl(is_I2, active_edges, Event[k-1],  AMI_row, AMI_col, AMI_keys, NG, w_EN)
    return Time, is_I2, active_edges

#%%
#function Jumps_Inf_cpl : a number of infection steps from Step_inf_cpl, 
#where jumps correspond to the period where no data is kept on the process
#the code is here still nearly exact, only the cost of storage is partly lifted

###INPUT
##t_f>1 : expected final time (initiation at time 1.)
#the actual one will be an approximation expressed through the number of iterations

##t_J : expected time of jump
#the actual one will be an approximation expressed through the number of iterations

##is_I : the boolean indicator of size 2xNI for individuals being I at this step,
#first for the fixed graph model, then for the mean-field

##AMI : Adjacency matrix in sparse coo format,
#the crucial information will be decomposed once and for all selection purposes
# (rows : "Origin"), (columns : "Destination"), (keys : conjonction for rapid identification)


##gamma : remission rate (constant)

###OUTPUT
#Time : the timing of each step, size N_it, infection start at time 1

##is_IT : the boolean indicator of size (N_it x 2 x NI) for individuals being I 
#at each time step
#first for the fixed graph model, then for the mean-field
#specified by individual

#%%
def Jumps_Inf_cpl(t_f, t_J, is_I, AMI, gamma, epsN, wI0, wE0):
    #calibration of the duration in terms of a number of iterations
    N_it = 1000
    Time, is_IT = Total_Inf_cpl(N_it, is_I, AMI, gamma, epsN, wI0, wE0)
    if not(np.any(is_IT[-1])):#the infection had stopped previously
        Nf = np.sum(Time >0)
        print("end of the infection at time {:.2f}".format(Time[Nf-1]))
        return Time[:Nf], is_IT[:Nf]
    else: 
        NI = is_I.shape[1]
        N_J = int(t_J/(Time[-1]-1)*N_it) 
        N_it2= int((t_f-1) /t_J)+1
        #calibration completed, we go on with the initialisation
        Time = np.zeros(N_it2)
        is_IT = np.zeros((N_it2, 2, NI), dtype = bool)
        k = 1
        Time[0] = 1.
        is_IT[0] = np.copy(is_I)
        #for the choice of the potential events
        vp = np.concatenate((np.repeat(gamma, NI), AMI.data, NI * wI0 * wE0*np.ones(1)))
        #change of the time scale through the rate of events
        Svp = np.sum(vp)
        vp = vp/Svp
        #iteration procedure of steps without memory
        k = 1
        active_edges = 
        #sp.dok_matrix((NI, NI), dtype = bool)
        NG = AMI.count_nonzero()
        AMI_row = AMI.row
        AMI_col = AMI.col
        AMI_keys = AMI.todok().keys()
        w_EN = wE0/(NI*epsN)
        while (k < N_it2)&(np.any(is_IT[k-1])):
            Time[k], is_IT[k], active_edges = Partial_Inf_cpl(N_J, Time[k-1], is_IT[k-1], active_edges,  AMI_row, AMI_col, AMI_keys, NG, w_EN, Svp, vp)
            k = k+1
        Nf = np.sum(Time >0)
        return Time[:Nf], is_IT[:Nf]
 
#%%
##Graph exploration for connectivity
###INPUT

##AMI : Adjacency matrix in sparse coo format
# (rows : "Origin"), (columns : "Destination"), (data : Intensities)


##NI : the number of individuals

###OUTPUT

#the number of connected components

#the function from the set of individuals to the set of components
#%%
def Graph_connect_cpl(AMI, NI):
    CoCom = np.arange(NI)
    Edge = np.ones((2, AMI.row.size), dtype = int)
    Edge[0] = np.array(AMI.row)
    Edge[1] = np.array(AMI.col)
    #since the graph is undirected, I remove duplicates
    I = (Edge[0]<Edge[1])
    Edge = Edge[:, I]
    while Edge.size >1:
        #I act on the first edge still on the list,
        #by fusing the components at the two extremities
        #according to the smallest index
        Com1 = CoCom[Edge[0,0]]
        #the index of the connected component at the origin
        Com2 = CoCom[Edge[1, 0]]
        #the index of the connected component at the destination
        if Com1 < Com2:
            CoCom[CoCom == Com2] = Com1
        else:
            CoCom[CoCom == Com1] = Com2
        #then I can remove all edges that bring no new connexion
        Edge = Edge[:, CoCom[Edge[0, :]]!=CoCom[Edge[1, :]] ]
    return np.unique(CoCom).size, CoCom
#%%
