# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:20:05 2022

@author: Aurélien Velleret
"""


#%%
import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import time 
import pandas as pd


from sparse_cpl_functions import AMI_Def_coo_1D, Jumps_Inf_cpl, Graph_connect_cpl
#%%
from sparse_data_selection import summary_parameters, summary_per_pop_size
version = summary_parameters.loc[0, "version"]
summary_parameters.to_csv('summary_parameters_v{:d}.csv'.format(version))
summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))

#%%

#NI =input("How many individuals are to be considered?\n")
NI = summary_parameters.loc[0, "Nbr_indiv_ref"]
uA = summary_parameters.loc[0,'p_init_infected'] 
is_I = np.zeros((2, NI), dtype = bool)
is_I[0] = npr.choice([False, True], size = NI, p = [1-uA, uA])
#same initial condition for the mean-field model in the coupling
is_I[1] = is_I[0]
gamma = summary_parameters.loc[0,'rate_remission']

#epsN =input("What is the scale of meeting interactions?\n")
#alpha =input("What is the power-law for this scaling?\n")
alpha = summary_parameters.loc[0,'alpha']
epsN = 1/NI**alpha

# #wI0 =input("How likely it is for two connected individuals to transmit an infection?\n")
wI0 = summary_parameters.loc[0,'w_I_ref']
# #wE0 =input("How likely it is for two individuals to be connected?\n")
wE0 = summary_parameters.loc[0,'w_E_ref'] 

t_f = summary_parameters.loc[0,'final_time'] 
t_J = summary_parameters.loc[0,'duration_btw_observations']
#%%

#Control study with the other simulations
AMI = AMI_Def_coo_1D(NI, epsN, wE0,  wI0)

NC, CoCom = Graph_connect_cpl(AMI, NI)
print("Number of connected components : {:d}".format(NC))
summary_parameters['Conn_Comp_ref'] = NC

A, CodCom = np.unique(CoCom, return_counts=True)
ordj= np.argsort(CodCom)
print("Size of the giant connected component : {:d}".format(CodCom[ordj[-1]]))
summary_parameters['Size_giant_Comp'] = CodCom[ordj[-1]]
#In this case, except the main component, only 19 isolated individuals are added


 
start_time = time.process_time()
Time, is_IT = Jumps_Inf_cpl(t_f, t_J, is_I, AMI, gamma, epsN, wI0, wE0)
time_exec = time.process_time()  - start_time
#%%
summary_per_time = pd.DataFrame(Time, columns = ['Current_time']) 
summary_per_time["proportion_infected_FG"] = np.sum(is_IT[:, 0], axis = 1)/is_IT[:,0].shape[1]
summary_per_time["proportion_infected_MG"] = np.sum(is_IT[:, 1], axis = 1)/is_IT[:,1].shape[1]
summary_per_time["coupling_error"] = np.sum(is_IT[:, 0]!=is_IT[:, 1], axis = 1)/is_IT[:,0].shape[1]
summary_per_time.to_csv("summary_per_time_v{:d}.csv".format(version))
#FG for "Fixed_graph", "MG" for "Mixed Graph"

N_it = Time.size
plt.clf()
plt.plot(Time, summary_per_time["proportion_infected_FG"], label = "proportion of infected, FG")
plt.plot(Time, summary_per_time["proportion_infected_MG"], label = "proportion of infected, MG")
plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(N_it), label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_v{:d}.png".format(version))
plt.show()

#%%
plt.clf()
plt.plot(Time, np.abs(summary_per_time["proportion_infected_FG"]\
        - summary_per_time["proportion_infected_MG"]), label = "difference in proportion")
plt.plot(Time, summary_per_time["coupling_error"], label = "difference in individual states")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_cpl_v{:d}.png".format(version))
plt.show()

#%%

t_o = summary_parameters.loc[0, 'duration_bfr_start_observations'] 
NIK = np.array(summary_per_pop_size['Nbr_indiv'])
NN = NIK.size

Mean_I_FG = np.zeros(NN)
Std_I_FG = np.zeros(NN)
Min_I_FG = np.zeros(NN)
Max_I_FG = np.zeros(NN)

Mean_I_MG = np.zeros(NN)
Std_I_MG = np.zeros(NN)
Min_I_MG = np.zeros(NN)
Max_I_MG = np.zeros(NN)

Mean_I_GC_FG = np.zeros(NN)
Std_I_GC_FG = np.zeros(NN)

Mean_I_GC_MG = np.zeros(NN)
Std_I_GC_MG = np.zeros(NN)
#%%

plt.clf()

for k in np.arange(NN):
    #the function int is called to removed the integer being int32
    #because AMI exploits NIk*NIk in possibly larger format
    NIk = int(NIK[k])
    AMI = AMI_Def_coo_1D(NIk, epsN/(NIk/NI)**alpha, wE0,  wI0)
    is_I = npr.choice([False, True], size = NIk, p = [1-uA, uA])
    Time, is_IT = Jumps_Inf_cpl(t_f, t_J, is_I, AMI, gamma, epsN, wI0, wE0)
    Mean_I_FG[k] = np.mean(is_IT[int(t_o/t_J):, 0])
    summary_per_pop_size.loc[k, 'mean_time_infected_FG'] = Mean_I_FG[k]
    Std_I_FG[k] = np.std(np.mean(is_IT[int(t_o/t_J):, 0], axis = 1))
    summary_per_pop_size.loc[k, 'std_per_time_of_time_infected_FG'] = Std_I_FG[k]
    Min_I_FG[k] = np.min(np.mean(is_IT[int(t_o/t_J):, 0], axis = 1))
    summary_per_pop_size.loc[k, 'min_prop_over_time_FG'] = Min_I_FG[k]
    Max_I_FG[k] = np.max(np.mean(is_IT[int(t_o/t_J):, 0], axis = 1))
    summary_per_pop_size.loc[k, 'max_prop_over_time_FG'] = Max_I_FG[k]
    #
    Mean_I_MG[k] = np.mean(is_IT[int(t_o/t_J):, 1])
    summary_per_pop_size.loc[k, 'mean_time_infected_MG'] = Mean_I_MG[k]
    Std_I_MG[k] = np.std(np.mean(is_IT[int(t_o/t_J):, 1], axis = 1))
    summary_per_pop_size.loc[k, 'std_per_time_of_time_infected_MG'] = Std_I_MG[k]
    Min_I_MG[k] = np.min(np.mean(is_IT[int(t_o/t_J):, 1], axis = 1))
    summary_per_pop_size.loc[k, 'min_prop_over_time_MG'] = Min_I_MG[k]
    Max_I_MG[k] = np.max(np.mean(is_IT[int(t_o/t_J):, 1], axis = 1))
    summary_per_pop_size.loc[k, 'max_prop_over_time_MG'] = Max_I_MG[k]    
    #
    summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))
    #
    NC, CoCom = Graph_connect_cpl(AMI, NIk)
    print("Number of connected components at population {:.3e}: {:d}".format(NIk, NC))
    summary_per_pop_size.loc[k, 'Nbr_conn_comp'] = NC
    A, CodCom = np.unique(CoCom, return_counts=True)
    #
    ID_giant = np.argsort(CodCom)[-1]
    In_giant = (CoCom == A[ID_giant])
    summary_per_pop_size.loc[k, 'Size_conn_comp'] = np.sum(In_giant)
    #
    Mean_I_GC_FG[k] = np.mean(is_IT[int(t_o/t_J):, 0][:, In_giant])
    summary_per_pop_size.loc[k, 'mean_time_infected_in_giant_FG'] = Mean_I_GC_FG[k]
    Std_I_GC_FG[k] = np.std(np.mean(is_IT[int(t_o/t_J):, 0][:, In_giant], axis = 1))
    summary_per_pop_size.loc[k, 'std_per_time_of_time_infected_in_giant_FG'] = Std_I_GC_FG[k]
    #
    Mean_I_GC_MG[k] = np.mean(is_IT[int(t_o/t_J):, 1][:, In_giant])
    summary_per_pop_size.loc[k, 'mean_time_infected_in_giant_MG'] = Mean_I_GC_MG[k]
    Std_I_GC_MG[k] = np.std(np.mean(is_IT[int(t_o/t_J):, 1][:, In_giant], axis = 1))
    summary_per_pop_size.loc[k, 'std_per_time_of_time_infected_in_giant_MG'] = Std_I_GC_MG[k]
    #    
    summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))
    plt.plot(Time, np.sum(is_IT[:, 0], axis = 1)/is_IT[:, 0].shape[1])
            # label = "NI = {:.2f}".format(NIk))
    

plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(Time.size), \
         label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_{:d}_diff_N.png".format(version))
plt.show()


# AMI.to_csv("AMI_v{:d}.csv".format(version))


#%%
plt.clf()
plt.plot(np.log10(NIK), Mean_I_FG, "b*--", label = "avg")
plt.plot(np.log10(NIK), Mean_I_FG-2*Std_I_FG, "vr:")
plt.plot(np.log10(NIK), Mean_I_FG+2*Std_I_FG, "^r:", label = "2 std")
plt.vlines(np.log10(NIK), \
          ymin = Mean_I_FG-2*Std_I_FG,\
        ymax = Mean_I_FG+2*Std_I_FG,  colors = "red")
plt.plot(np.log10(NIK), Min_I_FG, "vm:", alpha = 0.3)
plt.plot(np.log10(NIK), Max_I_FG, "^m:", alpha = 0.3, label = "range")
plt.vlines(np.log10(NIK), \
          ymin = Min_I_FG,\
        ymax = Max_I_FG, alpha = 0.3,  colors = "m")
plt.plot(np.log10(NIK), (1-gamma/(wI0*wE0))*np.ones(NN), "k+-", label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Log population size")
plt.ylabel("Equilibrium fluctuations")
plt.legend()
plt.title("Distribution of infected individuals\n \
as a function of population size")
plt.savefig("conc_error_v{:d}_diff_N.png".format(version))
plt.show()

#%%
X1 = np.log10(NIK)
Y1 = np.log10(Std_I_FG)
slope1 = (np.sum(X1*Y1)-np.mean(X1)*np.sum(Y1))/ (np.size(X1) * np.var(X1))
summary_parameters["slope_logsc_std_with_N"] = slope1
X2 = np.log10(NIK)
Y2 = np.log10(Std_I_GC_FG)
slope2 = (np.sum(X2*Y2)-np.mean(X2)*np.sum(Y2))/ (np.size(X2) * np.var(X2))
summary_parameters["slope_logsc_std_in_giant_with_N"] = slope2

plt.clf()
plt.plot(X1, Y1, "m*", label = "gnl std")
plt.plot(X1, np.mean(Y1) + slope1*(X1 - np.mean(X1)), "m+--", label = "slope = {:.2f}".format(slope1))
plt.plot(np.log10(NIK), np.log10(Std_I_GC_FG), "r*", label = "std in giant")
plt.plot(X2, np.mean(Y2) + slope2*(X2 - np.mean(X2)), "r+--", label = "slope = {:.2f}".format(slope2))

plt.xlabel("Log population size")
plt.ylabel("Temporal standard deviations")
plt.legend()
plt.title("The effect of population size on the temporal fluctuations\n\
in the numbers of infected")
plt.savefig("std_v{:d}_diff_N_giant.png".format(version))
plt.show()

#%%
X3 = np.log10(NIK)
Y3 = np.log10(np.abs(Mean_I_FG - 1+ gamma/(wI0*wE0)))
slope3 = (np.sum(X3*Y3)-np.mean(X3)*np.sum(Y3))/ (np.size(X3) * np.var(X3))
summary_parameters["slope_logsc_prop_error_with_N"] = slope3
X4 = np.log10(NIK)
Y4 = np.log10(np.abs(Mean_I_GC_FG - 1+ gamma/(wI0*wE0)))
slope4 = (np.sum(X4*Y4)-np.mean(X4)*np.sum(Y4))/ (np.size(X4) * np.var(X4))
summary_parameters["slope_logsc_prop_error_in_giant_with_N"] = slope4

plt.clf()
plt.plot(X3, Y3, "b*:",  label = "cvg")
plt.plot(X3, np.mean(Y3) + slope3*(X3 - np.mean(X3)), "b+--", label = "slope = {:.2f}".format(slope3))
plt.plot(X4, Y4, "c*:", label = "cvg in giant")
plt.plot(X4, np.mean(Y4) + slope4*(X4 - np.mean(X4)), "c+--", label = "slope = {:.2f}".format(slope4))
plt.plot(X3, np.mean(Y3) - alpha*(X3 - np.mean(X3)), "k+--", label = "predicted slope = {:.2f}".format(-alpha))


plt.xlabel("Log population size")
plt.ylabel("Log Discrepancy of the averages")
plt.legend()
plt.title("The effect of population size on the convergence\n\
of the numbers of infected")
plt.savefig("mean_v{:d}_diff_N_giant.png".format(version))
plt.show()

#%%
plt.clf()
plt.plot(np.log10(NIK), Mean_I_GC_FG, "b*--", label = "avg in GC")
plt.plot(np.log10(NIK), Mean_I_GC_FG-2*Std_I_GC_FG, "vr:")
plt.plot(np.log10(NIK), Mean_I_GC_FG+2*Std_I_GC_FG, "^r:")
plt.vlines(np.log10(NIK), \
          ymin = Mean_I_GC_FG-2*Std_I_GC_FG,\
        ymax = Mean_I_GC_FG+2*Std_I_GC_FG,\
        colors = "red", label = "avg@2sig in GC")
plt.plot(np.log10(NIK), Mean_I_FG, "c*--", alpha = 0.3, label = "avg")
plt.plot(np.log10(NIK), Mean_I_FG-2*Std_I_FG,  "mv:", alpha = 0.3)
plt.plot(np.log10(NIK), Mean_I_FG+2*Std_I_FG, "m^:", alpha = 0.3)
plt.plot(np.log10(NIK), (1-gamma/(wI0*wE0))*np.ones(NN), "k+-", label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Log population size")
plt.ylabel("Equilibrium fluctuations")
plt.legend()
plt.title("Distribution of infected individuals in the giant\n \
as a function of population size")
plt.savefig("conc_error_v{:d}_diff_N_giant.png".format(version))
plt.show()

#%%
I_ev = (Time>t_o)

Mean_indiv_FG = np.mean(is_IT[I_ev, 0], axis = 0)
Std_indiv_FG = np.std(is_IT[I_ev, 0], axis = 0)
In_giant = (Mean_indiv_FG>0)
summary_per_indiv = pd.DataFrame(Mean_indiv_FG, columns = ['Proportion_time_infected_FG'])
summary_per_indiv['std_time_infected_FG'] = Std_indiv_FG
summary_per_indiv['Is_in_giant_FG'] = In_giant

plt.clf()
plt.hist(Mean_indiv_FG[Mean_indiv_FG>0], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv_FG[Mean_indiv_FG>0]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individual infected in the long run")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend(title = "FG")
plt.savefig("Mean_time_inf_v{:d}.png".format(version))
plt.show()

summary_per_indiv.to_csv('summary_per_indiv_v{:d}.csv'.format(version))

#%%
plt.clf()
plt.hist(Mean_indiv_FG[In_giant], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv_FG[In_giant]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individuals in the giant component")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend(title = "FG")
plt.savefig("Mean_time_inf_giant_v{:d}_FG.png".format(version))
plt.show()

#%%

iD, degree_C = np.unique(AMI[["Origin"]], return_counts =True)
degree = np.zeros(Mean_indiv_FG.size)
degree[iD] = degree_C

plt.clf()
plt.hist(degree)
plt.title("Degree distribution")
#plt.xlabel("degree")
plt.savefig("degree_distribution_v{:d}.png".format(version))
plt.show()
#%%
plt.clf()
plt.plot(degree[Mean_indiv_FG>0], Mean_indiv_FG[Mean_indiv_FG>0], 'r+')
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")

#%%
deg_m = np.min(degree[In_giant])-0.5
deg_M = np.max(degree[In_giant])+0.5
L = int(min(deg_M-deg_m+1, 50))

plt.clf()
plt.hist2d(degree[Mean_indiv_FG>0], Mean_indiv_FG[Mean_indiv_FG>0], \
         bins = [np.linspace(deg_m, deg_M, L), np.linspace(0.5, 1, 30)], density = True)
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")
plt.colorbar()
plt.savefig("Time_inf_vs_degree_v{:d}_FG.png".format(version))
plt.show()
#%%
deg_av = np.mean(degree[In_giant])
deg_std = np.std(degree[In_giant])
kwargs = dict(histtype='stepfilled', alpha=0.4, density=True, bins = np.linspace(0.4, 1, 20), ec="k")
degT = np.array([deg_av-2*deg_std, deg_av, deg_av+2*deg_std], dtype = int)
Nv = degT.size

plt.clf()
for i in np.arange(Nv):    
    plt.hist(Mean_indiv_FG[degree == degT[i]],**kwargs, \
             fc=(i/Nv, 1-i/Nv, 1-i/Nv, 0.3) , label = "degree {:d}".format(degT[i]))
plt.legend(loc ="upper left")
plt.title("Infection level as a function of the degree")
plt.xlabel("mean time being infected")
plt.ylabel("normalized density for each degree")
plt.savefig("Time_inf_by_degree_v{:d}-1_FG.png".format(version))
plt.show()

#%%

Mean_indiv_MG = np.mean(is_IT[I_ev, 1], axis = 0)
Std_indiv_MG = np.std(is_IT[I_ev, 1], axis = 0)
summary_per_indiv = pd.DataFrame(Mean_indiv_MG, columns = ['Proportion_time_infected_MG'])
summary_per_indiv['std_time_infected_MG'] = Std_indiv_MG
summary_per_indiv['Is_in_giant_MG'] = In_giant

plt.clf()
plt.hist(Mean_indiv_MG[Mean_indiv_MG>0], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv_MG[Mean_indiv_MG>0]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individual infected in the long run")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend(title = "MG")
plt.savefig("Mean_time_inf_v{:d}.png".format(version))
plt.show()

summary_per_indiv.to_csv('summary_per_indiv_v{:d}.csv'.format(version))

#%%
plt.clf()
plt.hist(Mean_indiv_MG[In_giant], bins = np.linspace(0, 1, 30), fc=(0, 1, 1, 0.5),  density = True)
plt.axvline(x=np.mean(Mean_indiv_MG[In_giant]), color = "b", label ="empirical mean")
plt.axvline(x=(1-gamma/(wI0*wE0)), color = "r", label ="expected mean")
plt.title("Distribution of mean time being infected\n \
for individuals in the giant component")
plt.xlabel("mean time being infected")
plt.ylabel("density of the empirical distribution")
plt.legend(title = "MG")
plt.savefig("Mean_time_inf_giant_v{:d}_MG.png".format(version))
plt.show()

#%%

iD, degree_C = np.unique(AMI[["Origin"]], return_counts =True)
degree = np.zeros(Mean_indiv_MG.size)
degree[iD] = degree_C

plt.clf()
plt.hist(degree)
plt.title("Degree distribution")
#plt.xlabel("degree")
plt.savefig("degree_distribution_v{:d}.png".format(version))
plt.show()
#%%
plt.clf()
plt.plot(degree[Mean_indiv_MG>0], Mean_indiv_MG[Mean_indiv_MG>0], 'r+')
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")

#%%
deg_m = np.min(degree[In_giant])-0.5
deg_M = np.max(degree[In_giant])+0.5
L = int(min(deg_M-deg_m+1, 50))

plt.clf()
plt.hist2d(degree[Mean_indiv_MG>0], Mean_indiv_MG[Mean_indiv_MG>0], \
         bins = [np.linspace(deg_m, deg_M, L), np.linspace(0.5, 1, 30)], density = True)
plt.ylabel("mean time being infected")
plt.xlabel("individual degree")
plt.title("Infection level as a function of the degree")
plt.colorbar()
plt.savefig("Time_inf_vs_degree_v{:d}_MG.png".format(version))
plt.show()
#%%
deg_av = np.mean(degree[In_giant])
deg_std = np.std(degree[In_giant])
kwargs = dict(histtype='stepfilled', alpha=0.4, density=True, bins = np.linspace(0.4, 1, 20), ec="k")
degT = np.array([deg_av-2*deg_std, deg_av, deg_av+2*deg_std], dtype = int)
Nv = degT.size

plt.clf()
for i in np.arange(Nv):    
    plt.hist(Mean_indiv_MG[degree == degT[i]],**kwargs, \
             fc=(i/Nv, 1-i/Nv, 1-i/Nv, 0.3) , label = "degree {:d}".format(degT[i]))
plt.legend(loc ="upper left")
plt.title("Infection level as a function of the degree")
plt.xlabel("mean time being infected")
plt.ylabel("normalized density for each degree")
plt.savefig("Time_inf_by_degree_v{:d}-1_MG.png".format(version))
plt.show()
#%%