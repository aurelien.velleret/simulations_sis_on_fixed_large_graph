# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:20:05 2022

@author: Aurélien Velleret
"""


#%% Initialisation

import numpy as np
import numpy.random as npr
import matplotlib.pyplot as plt
import time 
import pandas as pd


from sparse_functions import AMI_Def_1, Step_Inf, Total_Inf, Partial_Inf, Jumps_Inf, Graph_connect

#%% Data choice

from sparse_data_selection_alpha_var import summary_parameters, summary_per_pop_size, summary_per_alpha
version = summary_parameters.loc[0, "version"]
summary_parameters.to_csv('summary_parameters_v{:d}.csv'.format(version))
summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))

#%% Data extraction

#NI =input("How many individuals are to be considered?\n")
NI = summary_parameters.loc[0, "Nbr_indiv_ref"]
uA = summary_parameters.loc[0,'p_init_infected'] 
is_I = npr.choice([False, True], size = NI, p = [1-uA, uA])
gamma = summary_parameters.loc[0,'rate_remission']

#epsN =input("What is the scale of meeting interactions?\n")
#alpha =input("What is the power-law for this scaling?\n")
alpha = summary_parameters.loc[0,'alpha']
epsN = 1/NI**alpha

# #wI0 =input("How likely it is for two connected individuals to transmit an infection?\n")
wI0 = summary_parameters.loc[0,'w_I_ref']
# #wE0 =input("How likely it is for two individuals to be connected?\n")
wE0 = summary_parameters.loc[0,'w_E_ref'] 

t_f = summary_parameters.loc[0,'final_time'] 
t_J = summary_parameters.loc[0,'duration_btw_observations']

NN = summary_parameters.loc[0,'Nbr_pop_size']
N_Al = summary_parameters.loc[0, 'Nbr_alpha_val']

#%% Control study with the other simulations
AMI = AMI_Def_1(NI, epsN, wE0,  wI0)

NC, CoCom = Graph_connect(AMI, NI)
print("Number of connected components : {:d}".format(NC))
summary_parameters['Conn_Comp_ref'] = NC

A, CodCom = np.unique(CoCom, return_counts=True)
ordj= np.argsort(CodCom)
print("Size of the giant connected component : {:d}".format(CodCom[ordj[-1]]))
summary_parameters['Size_giant_Comp'] = CodCom[ordj[-1]]
#In this case, except the main component, only 19 isolated individuals are added


 
start_time = time.process_time()
Time, is_IT = Jumps_Inf(t_f, t_J, is_I, AMI, gamma)
time_exec = time.process_time()  - start_time

#%% Plot dist_inf_v{:d}

summary_per_time = pd.DataFrame(Time, columns = ['Current_time']) 
summary_per_time["proportion_infected"] = np.sum(is_IT, axis = 1)/is_IT.shape[1]
summary_per_time.to_csv("summary_per_time_v{:d}.csv".format(version))


N_it = Time.size
plt.clf()
plt.plot(Time, summary_per_time["proportion_infected"], label = "proportion of infected")
plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(N_it), label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_v{:d}.png".format(version))
plt.show()

#%% Reinitialisation of the data arrays

t_o = summary_parameters.loc[0, 'duration_bfr_start_observations'] 
NIK = np.array(summary_per_pop_size['Nbr_indiv'])
Nr = NIK.size

MeanI = np.zeros(Nr)
StdI = np.zeros(Nr)
MinI = np.zeros(Nr)
MaxI = np.zeros(Nr)
MeanIG = np.zeros(Nr)
StdIG = np.zeros(Nr)

k = 0

#%% Runs of simulations

plt.clf()


while k < Nr:
    #the function int is called to removed the integer being int32
    #because AMI exploits NIk*NIk in possibly larger format
    NIk = int(NIK[k])
    alpha_k = summary_per_pop_size.loc[k, 'alpha']
    AMI = AMI_Def_1(NIk, epsN/(NIk/NI)**alpha_k, wE0,  wI0)
    is_I = npr.choice([False, True], size = NIk, p = [1-uA, uA])
    #data production:
    Time, is_IT = Jumps_Inf(t_f, t_J, is_I, AMI, gamma)
    #data collection
    MeanI[k] = np.mean(is_IT[int(t_o/t_J):])
    summary_per_pop_size.loc[k, 'mean_time_infected'] = MeanI[k]
    StdI[k] = np.std(np.mean(is_IT[int(t_o/t_J):], axis = 1))
    summary_per_pop_size.loc[k, 'std_per_time_of_time_infected'] = StdI[k]
    MinI[k] = np.min(np.mean(is_IT[int(t_o/t_J):], axis = 1))
    summary_per_pop_size.loc[k, 'min_prop_over_time'] = MinI[k]
    MaxI[k] = np.max(np.mean(is_IT[int(t_o/t_J):], axis = 1))
    summary_per_pop_size.loc[k, 'max_prop_over_time'] = MaxI[k]
    if (k%20 == 0):
        summary_per_pop_size.to_csv('summary_per_pop_size_v{:d}.csv'.format(version))
        if (k< NN):
            plt.plot(Time, np.sum(is_IT, axis = 1)/is_IT.shape[1])
            # label = "NI = {:.2f}".format(NIk))
    k = k+1
    

plt.plot(Time, summary_parameters.loc[0, 'expected_equilibrium']*np.ones(Time.size), \
         label = "expected equilibrium")
plt.ylim(0, 1)
plt.xlabel("Time")
plt.ylabel("Proportion of individuals")
plt.legend()
plt.title("Dynamics of the distribution of infected individuals")
plt.savefig("dist_inf_{:d}_diff_N.png".format(version))
plt.show()


# AMI.to_csv("AMI_v{:d}.csv".format(version))

#%% Data recollection

summary_per_pop_size = pd.read_csv("summary_per_pop_size_v{:d}.csv".format(version),
                                   index_col = 0)
NIK = summary_per_pop_size["Nbr_indiv"]
MeanI = summary_per_pop_size["mean_time_infected"]
StdI = summary_per_pop_size['std_per_time_of_time_infected']
MinI = summary_per_pop_size['min_prop_over_time']
MaxI = summary_per_pop_size['max_prop_over_time']

#%% Plot mean_v{:d}_Ndiff

for i in np.arange(N_Al):
    X3 = np.log10(NIK)[i*N_Al:(i+1)*N_Al]
    Y3 = np.log10(np.abs(MeanI - 1+ gamma/(wI0*wE0)))[i*N_Al:(i+1)*N_Al]
    slope3 = (np.sum(X3*Y3)-np.mean(X3)*np.sum(Y3))/ (np.size(X3) * np.var(X3))
    summary_per_alpha.loc[i, "slope_logsc_prop_error_with_N"] = slope3
    #
    alpha_i = summary_per_alpha.loc[i, "alpha"]
    plt.clf()
    plt.plot(X3, Y3, "b*:",  label = "cvg")
    plt.plot(X3, np.mean(Y3) + slope3*(X3 - np.mean(X3)), "b+--", label = "slope = {:.2f}".format(slope3))
    plt.plot(X3, np.mean(Y3) - alpha_i*(X3 - np.mean(X3)), "k+--", label = "predicted slope = {:.2f}".format(-alpha))
    #
    plt.xlabel("Log population size")
    plt.ylabel("Log Discrepancy of the averages")
    plt.legend(title = "alpha = {.2f}".format(alpha_i))
    plt.title("The effect of population size on the convergence\n\
    of the numbers of infected")
    plt.savefig("mean_v{:d}_a{:d}_diff_N_giant.png".format(version, i))
    plt.show()


#%% End

